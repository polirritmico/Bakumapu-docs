%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Intro

\section{Introducción}\label{intro:introduccion}

El presente documento busca ser una herramienta de diseño, un mapa de ruta que encamine tanto el desarrollo técnico del software en general como la implementación del código en el motor de juegos \textbf{Godot}. Pretende servir de ayuda o consulta para detalles específicos y servir de referencia para otorgar una visión general del funcionamiento interno del juego. Para facilitar el acceso a la información relevante se agrupan los contenidos en secciones afines, y se añade un completo índice de contenidos junto a referencias cruzadas a las distintas secciones relevantes dentro del mismo texto.

El documento incluye información con respecto al flujo de trabajo (\nameref{flujo:metodologia-kanban}, \nameref{flujo:modelo-de-ramas}), a la propuesta de diseño o arquitectura (\nameref{modelado:modelado-del-software}), al uso de sistemas de control \lsc{GIT} (\nameref{flujo:modelo-de-ramas}), a consideraciones de \nameref{i18n:internacionalizacion} y a muchos otros detalles relevantes para un software de esta magnitud y complejidad.

\subsection{Cómo usar este documento}\label{intro:como-usar-el-documento}
Lo ideal es una primera lectura completa para hacerse de una noción general del funcionamiento del software o lo que se espera lograr en cuanto a su diseño. Posteriormente la idea es usarlo a modo de referencia o mapa de ruta antes y durante la implementación del código.

El texto estará disponible para su modificación en el formato \lsc{TEX} en el \href{https://github.com/polirritmico/Bakumapu-docs}{repositorio de documentación del proyecto} y además estará disponible para su lectura en \href{https://polirritmico.github.io/Bakumapu-docs/}{\lsc{HTML}} y en \href{https://github.com/polirritmico/Bakumapu-docs/blob/main/main.pdf}{\lsc{PDF}}.

El manejo del documento se abordará con más detalle en el apartado \nameref{flujo:documento-de-diseno}.

\subsection{Información importante}
\noindent Los aspectos más relevantes si se busca pasar rápidamente a la implementación de código son los siguientes:

\begin{enumerate}
\item \nameref{principios:TDD} y \nameref{principios:refactorizacion-rendimiento-limpieza}.

\item \nameref{flujo:metodologia-kanban}, \nameref{flujo:repositorio}, \nameref{flujo:documento-de-diseno} y \nameref{flujo:documentacion-en-codigo}.

\item \nameref{modelado:funcionamiento-general} y \nameref{modelado:funcionamiento-de-managers}.

\item \nameref{organizacion:organizacion-del-proyecto}.
\end{enumerate}

\subsection{Lista de requerimientos}\label{intro:lista-de-requerimientos}
A continuación se presenta la lista inicial de requerimientos y consideraciones del programa. Importante destacar que este listado probablemente cambie a lo largo del desarrollo, además que las distintas implementaciones quizás generen nuevos requisitos o hagan obsoletos otros.

\begin{enumerate}%[noitemsep]
	\item Enfoque en la facilidad de agregar y editar contenido.
	\begin{enumerate}[noitemsep]
		\item Locaciones.
		\item Quests.
		\item Ítems.
		\item Personajes.
		\item Cutscenes.
		\item GUI.
	\end{enumerate}
	\item Multiplataforma: Inicialmente PC y Android.
	\item Multilenguaje: Español e inglés (en primera instancia).
	\item Pixelart.
	\item Isométrico 2:1.
	\item Relación de aspecto 16:9.
	\item Resolución ajustable a dispositivos Android. En \lsc{PC} a 720p y 1080p.
	\item Tasa de refresco idealmente de $\geq60$~\lsc{FPS}.
	\item Ajuste dinámico de la velocidad del juego.
	\item Ajuste de stats de ítems.
	\item Mixes de audio (estéreo y 5.1):
	\begin{enumerate}[noitemsep]
		\item Master.
		\item Música.
		\item Ambiente.
		\item SFX.
		\item Ataques.
		\item Props.
		\item UI.
	\end{enumerate}
	\item Seguimiento de las decisiones en las quests (quests path).
	\item States de guardado solo en locaciones del mapa.
	\begin{enumerate}[noitemsep]
		\item Ítems.
		\item EXP.
		\item HP y mana.
		\item Ubicación.
		\item Active Quests.
		\item Quest path.
	\end{enumerate}
	\item Tácticas y equipo de la party.
	\item IA de tácticas de la party.
	\item IA enemiga.
	\item Pathfinding de \lsc{NPC}.
	\item Sistema de magia y poderes.
	\item Sistema de partículas para ataques y locaciones.
	\item Sistema de iluminación.
	\item Interacción con props.
	\item Dificultad ajustable en menú.
\end{enumerate}

\subsection{Listado de software y herramientas de producción}\label{intro:software-y-herramientas}

\subsubsection{Godot v3.3}
Se utilizará como herramienta principal de desarrollo el motor de videojuegos multiplataforma open source \textbf{Godot} en su rama 3.3 estable. Ante el lanzamiento de la versión 4.0 se estudiará la migración del proyecto.

\subsubsection*{\small GUT 7.1.0.}
Plugin de \textbf{Godot} para escribir y controlar las pruebas de los scripts. Instalable desde la propia interfaz de Godot (tutorial \href{https://www.youtube.com/watch?v=5DrhMiuLRl0}{aquí}). Más información en el apartado \nameref{principios:TDD}.

\subsubsection{Trello}
Se utilizará la plataforma \textbf{Trello} para coordinar todo el desarrollo y el flujo de trabajo del equipo siguiendo la \emph{Metodología Kanban}. Más información en el apartado \nameref{flujo:tablero-kanban}.

\subsubsection{GIT y Github}
El código estará alojado en un repositorio \lsc{GIT} en \textbf{Github} en la siguiente dirección: \url{https://github.com/polirritmico/Bakumapu}. Todos los participantes deben tener una cuenta con permisos.

El control de versiones se explicará en detalle en el apartado \nameref{flujo:repositorio}.

\subsubsection{Google Suite}
Por conveniencia, tanto los archivos para la organización del proyecto así como los de diseño artístico del juego, se realizarán principalmente en la suite de Google. Más información en el apartado \nameref{flujo:google-drive}.

\subsubsection{LaTeX}
Para realizar el presente documento se ha utilizado \LaTeXe\ (del paquete TeX Live 2021) en el editor TeXstudio v3.1.2. Como alternativa se encuentra la plataforma online \href{https://www.overleaf.com/}{Overleaf} (gratuita con registro). En cualquier caso, un archivo \lsc{TEX} es simplemente un archivo de texto plano, por lo que para su edición cualquier editor de textos sencillo debería funcionar sin inconvenientes.

Para su conversión a \lsc{HTML} se utilizará \textbf{make4ht} en su versión 0.3g. Si es necesario convertirlo a Markdown se recomienda tomar el mismo \lsc{HTML} y transformarlo con Pandoc. Más información en el apartado \nameref{flujo:documento-de-diseno}.

\subsubsection{Aseprite}
Se utilizará en su versión 1.2.25 para el trabajo en estilo pixel art de los sprites de tiles, props, animaciones, \lsc{GUI} y la gran mayoría de los elementos visuales del juego. Ejecutable compilado para Windows \href{https://drive.google.com/drive/folders/1DPhGeg7WzV9j81u3B5isgsXqoMtfd_Uv?usp=sharing}{aquí}.

\subsubsection{Krita}
En su rama 4.4 se utilizará principalmente como alternativa (no pixel art) para el diseño visual de los personajes, mapas, etc.

\subsubsection{Inkscape}
Se utilizará como herramienta de diseño vectorial en su versión 1.1.

\subsubsection{Finale 2014}
Se utilizará para toda la composición musical en formato \lsc{MIDI} y \lsc{MUSX}.

\subsubsection{Ableton Live}
Se utilizará la versión 10.1.4 para los \lsc{MIX}, masters y/o conversión de archivos \lsc{MIDI} a formato \lsc{WAV} estéreo y toda la producción de diseño sonoro del juego. Tomar nota en caso de utilizar VST específicos.

\subsubsection{FFmpeg}
El formato para los archivos de música, sonido ambiente y efectos será el \lsc{OGG}. Para su conversión se utilizará \href{http://ffmpeg.org/}{FFmpeg} en su última versión estable (actualmente la 4.4) con el encoder \texttt{libvorvis}. Los \lsc{OGG} deberán ser de 192~kbps a 44~khz:
\lstset{style=bash}
\begin{lstlisting}
$ ffmpeg -i ARCHIVO.wav -acodec libvorbis -q:a 6 ARCHIVO.ogg
\end{lstlisting}