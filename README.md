# :book: Bakumapu: Repositorio de documentación

La presente documentación está escrita en el formato LaTeX y exportada a HTML con [make4ht](https://ctan.org/pkg/make4ht).

Para su lectura seguir el siguiente enlace:
- https://polirritmico.github.io/Bakumapu-docs/

También está disponible el documento original en PDF [aquí](https://github.com/polirritmico/Bakumapu-docs/raw/main/main.pdf).

---

Toda la información relevante del proyecto dentro del documento.
